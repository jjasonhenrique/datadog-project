#terraform {
 # backend "s3" {
  #  bucket = "terraform-jason"
   # key    = "terraform-datadog.tfstate"
   # region = "us-east-1"
   # encrypt = true
 # }
#}

resource "datadog_monitor" "cpumonitor" {
  name = "cpu monitor"
  type = "metric alert"
  message = "CPU usage alert"
  query = "avg(last_1m):avg:system.cpu.system{*} by {host} > 60"
}
