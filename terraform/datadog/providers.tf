terraform {
  required_providers {
    datadog = {
      source = "Datadog/datadog"
    }
  }
}

provider "datadog" {
  api_key = var.API_DATADOG
  app_key = var.APP_DATADOG
  api_url = var.api_url
}
