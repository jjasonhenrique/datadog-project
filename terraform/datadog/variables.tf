 variable "API_DATADOG" {
   type        = string
}


variable "APP_DATADOG" {
  type        = string
}


variable "api_url" {
  type        = string
  description = "API do datadog" 
  default     = "https://app.datadoghq.com/"
}
